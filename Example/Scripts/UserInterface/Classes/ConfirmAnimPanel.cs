﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project.UI
{
    /* =================================== */
    /* "TWO BUTTONS" DEFAULT PANEL         */
    /* =================================== */

    [Serializable]
    public class ConfirmAnimPanel : BaseAnimPanel
    {
        #region VARIABLES

        [SerializeField] Button confirm_btn;

        public Action ConfirmAct;

        #endregion

        /* ============================================== */

        public override void Init()
        {
            base.Init();
            confirm_btn.ClickAction(ConfirmAct_Default);
        }

        /* ============================================== */

        protected void ConfirmAct_Default()
        {
            ConfirmAct?.Invoke();
            Show();
            ConfirmAct = () => { };
        }

    }
}