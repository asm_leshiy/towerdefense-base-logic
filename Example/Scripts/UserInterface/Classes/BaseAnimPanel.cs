﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project.UI
{
    /* =================================== */
    /* "ONE BUTTON" DEFAULT PANEL          */
    /* =================================== */

    [Serializable]
    public class BaseAnimPanel : BaseUI
    {
        #region VARIABLES

        public new Transform Tr { get; protected set; }

        [SerializeField] protected Animator Anim;
        [SerializeField] protected Text Title;
        [SerializeField] protected Text Msg;
        [SerializeField] protected Text Stats;
        [SerializeField] protected Text Value;

        const char SEPARATOR = '#';

        #endregion

        /* ============================================== */

        public override void Init()
        {
            Tr = Anim.transform;
            Msg.text = string.Empty;
            Stats.text = string.Empty;
            Value.text = string.Empty;

            base_btn.ClickAction(BaseAct_Default);
        }

        public override void Show()
        {
            if (!IsActive) Tr.SetAsLastSibling();

            Anim.SetTrigger("show");
            SoundMng.Play(SoundType.UI_SHOW);
        }

        /* ============================================== */

        public void Content(string title, string msg)
        {
            Title.text = title;
            Msg.text = msg;
            Stats.text = string.Empty;
            Value.text = string.Empty;
        }

        public static string Combine(string stats, string values) => stats + SEPARATOR + values;

        public void TwoColumnContent(string title, string combine_str)
        {
            Title.text = title;
            string[] array = combine_str.Split(SEPARATOR);
            Stats.text = array[0];
            Value.text = array[1];
            Msg.text = string.Empty;
        }
    }
}