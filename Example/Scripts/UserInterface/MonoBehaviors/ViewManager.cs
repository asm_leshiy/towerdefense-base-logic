﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

namespace ASM.Project.UI
{
    /* =================================== */
    /* MAIN USER INTERFACE GAME MANAGER    */
    /* =================================== */

    public class ViewManager : MonoBehaviour
    {
        #region VARIABLES

        public static ViewManager Instance { get; private set; }
        static GameManager Mng => GameManager.Instance;

        [SerializeField] SidebarPanel sidebar_pnl;
        [SerializeField] TopPanel top_pnl;
        [SerializeField] BuyTowerPanel buy_tower_pnl;
        [SerializeField] MessegePanel message_pnl;

        List<BaseUI> Layouts = new List<BaseUI>();

        #endregion

        /* ********************************************************** */

        #region UNITY BASICS

        private void Awake()
        {
            Instance = this;

            sidebar_pnl.Init();
            buy_tower_pnl.Init();
            message_pnl.Init();
        }

        private void Start()
        {
            top_pnl.Init();
        }

        #endregion

        /* ********************************************************** */

        public void ShowLayout<T>() where T : BaseUI
        {
            Layouts.ForEach(panel =>
            {
                if (panel.GetType() == typeof(T)) panel.Show();
                else panel.Show(false);
            });
        }

        public void ShowSidebar()
        {
            top_pnl.Show(false);
            ShowLayout<SidebarPanel>();
        }

        public void ShowTowerPanel(TowerController tower)
        {
            buy_tower_pnl.ConfirmAct = () => Mng.BuildTower(tower);
            string action = tower.Stats.Level == 0 ? "Buy" : "Upgrade";
            string content = string.Empty;

            TowerStats next = tower.Stats.NextLevelValues();
            if (Mng.UserGold < next.Cost)
            {
                content = "No gold - no tower!";
                message_pnl.Content($"{action} tower panel", content);
                ShowLayout<MessegePanel>();
            }
            else
            {
                content = BaseAnimPanel.Combine(tower.Stats.AsFormatedString, tower.Stats.NextLevelAsFormatedString());
                buy_tower_pnl.TwoColumnContent($"{action} tower panel", content);
                ShowLayout<BuyTowerPanel>();
            }
        }

        public void ShowResults(Result result)
        {
            message_pnl.BaseAct = () => Mng.Restart(result.Win);

            string title = string.Empty;

            if (result.Win)
            {
                if(result.CleanWin) title = "Congratulations! You win!";
                else title = "You win! But ... can be better!";
            }
            else
            {
                title = "Loooser!";
                SoundManager.Instance.Play(SoundType.GAME_OVER);
            }

            string stats = $"Units was killed :\nYour score :";
            string values = $"{result.Kills}\n{result.Score}";
            string content = BaseAnimPanel.Combine(stats, values);
            message_pnl.TwoColumnContent(title, content);
            ShowLayout<MessegePanel>();
        }


        #region SIDEBAR_PANEL

        [Serializable]
        public class SidebarPanel : BaseUI
        {
            #region VARIABLES

            [Space(5)]
            [Header("Local variables")]
            public Button records_btn;
            public Button quit_btn;
            public Animator anim;

            #endregion

            /* ============================================== */

            public override void Init()
            {
                Instance.Layouts.Add(this);

                base_btn.ClickAction(delegate () {
                    GameManager.Instance.Play();
                    Instance.top_pnl.Show(true);
                    Show();
                });

                records_btn.ClickAction(()=> { });

                quit_btn.ClickAction(delegate()
                {
                    GameManager.Instance.Quit();
                });

            }

            public override void Show()
            {
                if (!IsActive) Tr.SetAsLastSibling();

                anim.SetTrigger("show");
                SoundMng.Play(SoundType.UI_SHOW);
            }

            /* ============================================== */

        }

        #endregion

        #region GAME_LEVEL_PANELS

        [Serializable]
        public class TopPanel : BaseUI
        {
            #region VARIABLES

            [Space(5)]
            [Header("Local variables")]
            public Text best_scores_txt;
            public Text left_txt;
            public Text right_txt;

            #endregion

            /* ============================================== */

            public override void Init()
            {
                base.Init();
                left_txt.text = string.Empty;
                right_txt.text = string.Empty;
                base_btn.interactable = false;
                Show(false);
            }

            public override void Show(bool show)
            {
                if (show) Mng.OnUpdate += OnUpdate;
                else Mng.OnUpdate -= OnUpdate;

                base.Show(show);
            }

            /* ============================================== */

            public void OnUpdate()
            {
                best_scores_txt.text = BestScores(Mng.BestScores);
                left_txt.text = $"{Gold(Mng.UserGold)}\n{Score(Mng.UserScore)}";
                right_txt.text = $"{Defence(Mng.UserDefence)}\n{Waves(Mng.WaveCurrent, Mng.WaveMax)}";
            }

            public string BestScores(int scores) => $"Best scores : {scores.ToString()}";
            public string Gold(int gold) => $"Gold : {gold.ToString()}";
            public string Score(int score) => $"Scores : {score.ToString()}";
            public string Defence(int defence) => $"Defence : {defence.ToString()}";
            public string Waves(int waves_cur, int wave_max) => $"Waves : {waves_cur.ToString()} / {wave_max.ToString()}";
        }

        [Serializable]
        public class BuyTowerPanel : ConfirmAnimPanel
        {
            public override void Init()
            {
                base.Init();
                Instance.Layouts.Add(this);
            }
        }

        [Serializable]
        public class MessegePanel : BaseAnimPanel
        {
            public override void Init()
            {
                base.Init();
                Instance.Layouts.Add(this);
            }
        }

        #endregion

    }
}