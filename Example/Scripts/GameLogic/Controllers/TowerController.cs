﻿using UnityEngine;
using System.Collections;

namespace ASM.Project
{
    /* =================================== */
    /* TOWER CONTROLLER IN GAME SCENE      */
    /* =================================== */

    public class TowerController : MonoBehaviour
    {
        #region VARIABLES

        static GameManager Mng => GameManager.Instance;

        public TowerStats Stats { get; private set; }
        public Mob Target { get; set; }

        [SerializeField] GameObject tower_obj;
        [SerializeField] Transform spawn_tr;
        [SerializeField] Transform tower_tr;
        [SerializeField] Transform range_tr;

        Spawner<Bullet> spawner;
        Color32 color_norm;
        bool rotate;

        public bool IsActive => tower_obj.activeSelf;
        public Vector3 SpawnPos => spawn_tr.position;

        #endregion

        /* ********************************************************** */

        #region UNITY BASICS

        private void OnEnable() => Mng.Tower_Add(this);
        private void OnDisable() => Mng.Tower_Remove(this);

        private void Start()
        {
            spawner = new Spawner<Bullet>();
        }

        #endregion

        /* ********************************************************** */

        public void ToDefault()
        {
            tower_obj.SetActive(false);
            Stats = new TowerStats().Default();
        }

        public void OnModify()
        {
            tower_obj.SetActive(true);
            Stats = Stats.LevelUp();
            range_tr.localScale = new Vector3(Stats.Range * 2, Stats.Range * 2);
        }

        public bool HaveTarget()
        {
            if (Target != null && Target.IsActive) return true;

            Target = null;
            return false;
        }

        public bool IsAtRange(Vector3 pos, ref float dist)
        {
            if (HaveTarget()) return false;

            dist = Vector3.Distance(tower_tr.position, pos);
            return dist < Stats.Range;
        }

        public void TargetMonitoring()
        {
            if (!HaveTarget()) return;

            if (Vector3.Distance(tower_tr.position, Target.Pos) > Stats.Range) Target = null;
        }

        public void LookAtTarget()
        {
            if (!HaveTarget()) return;

            Vector3 target_dir = (Target.Pos + Vector3.up) - tower_tr.position;
            float step = Stats.RotSpeed * Time.deltaTime;
            Vector3 dir = Vector3.RotateTowards(tower_tr.forward, target_dir, step, step);
            Debug.DrawRay(tower_tr.position, dir * 10, Color.red);

            if (Vector3.Angle(target_dir, dir) > 5) rotate = true;
            else rotate = false;

            tower_tr.rotation = Quaternion.LookRotation(dir);

        }

        public void Shooting()
        {
            if (!IsActive || !HaveTarget() || rotate) return;

            spawner.Spawn(this);
        }
    }
}