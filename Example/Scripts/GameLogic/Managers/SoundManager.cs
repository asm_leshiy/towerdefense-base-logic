﻿using UnityEngine;

namespace ASM.Project
{
    using Data;

    public class SoundManager : MonoBehaviour
    {
        #region VARIABLES

        public static SoundManager Instance { get; private set; }

        [SerializeField] SoundCollection sounds;
        AudioSource[] sound_sourses = new AudioSource[3];

        #endregion

        /* ********************************************************** */

        #region UNITY BASICS

        private void Awake()
        {
            if (Instance != this) Instance = this;

            for (int i = 0; i < sound_sourses.Length; i++)
            {
                AudioSource sound = gameObject.AddComponent<AudioSource>();
                sound.playOnAwake = false;
                sound_sourses[i] = sound;
            }
        }

        #endregion

        /* ********************************************************** */

        string GetSound(SoundType key)
        {
            switch (key)
            {
                case SoundType.UI_SHOW: return "ui_show";
                case SoundType.BUTTON_CLICK: return "button_click";
                case SoundType.GAME_OVER: return "game_over";
            }

            throw new System.MissingFieldException();
        }
        public void Play(SoundType key)
        {
            string name = GetSound(key);

            AudioClip sound = null;
            for (int i = 0; i < sounds.Collection.Length; i++)
            {
                if (sounds.Collection[i].name != name) continue;
                sound = sounds.Collection[i];
                break;
            }

            if (sound == null)
            {
                Debug.LogError("[ASM] : Sound not found");
                return;
            }

            for (int i = 0; i < sound_sourses.Length; i++)
            {
                if (sound_sourses[i].isPlaying) continue;
                sound_sourses[i].clip = sound;
                sound_sourses[i].Play();
                break;
            }
        }

    }
}