﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

namespace ASM.Project
{
    using Random = UnityEngine.Random;
    using UI;

    /* =================================== */
    /* GAME MANAGER CLASS                  */
    /* =================================== */

    public class GameManager : MonoBehaviour
    {
        #region VARIABLES

        public static GameManager Instance { get; private set; }
        public bool GameIsRunning { get; private set; }
        public event Action OnUpdate;

        [SerializeField] Transform towers_ph;
        [SerializeField] Transform routes_ph;
        Transform bullets_ph;
        Transform mobs_ph;

        Vector3[][] routes;
        List<TowerController> towers = new List<TowerController>();
        Spawner<Mob> spawner_mob;
        GameData data;
        LevelRules rules;

        ViewManager View => ViewManager.Instance;
        public Vector3[] GetRout => routes[Random.Range(0, routes.Length)];
        public int BestScores => data.Score.Total;
        public int UserGold => data.Gold;
        public int UserScore => data.RealScore(rules.TotalKills);
        public int UserDefence => data.Defence;
        public int WaveCurrent => rules.WaveCurrent;
        public int WaveMax => rules.WaveMax;
        public float SpeedModirier => rules.SpeedModifier;

        #endregion

        /* ********************************************************** */

        #region ALL GAME LOGIC AND CONTROL LOCATED IN THIS REGION

        private void Awake()
        {
            if (Instance == null) Instance = this;
            else Destroy(gameObject);

            towers_ph.gameObject.SetActive(false);

            Routes_Init();
            Mobs_Init();
            Bullets_Init();
        }

        private IEnumerator Start()
        {
            Towers(true);
            View.ShowSidebar();

            while (true)
            {
                yield return new WaitWhile(() => !GameIsRunning);
                yield return new WaitWhile(() => !rules.CanSpawnNextWave);
                yield return SpawnNewMobWave();
                yield return new WaitForSeconds(1);
            }
        }

        private void Update()
        {
            if (!GameIsRunning) return;

            OnUpdate?.Invoke();

            OnInput();

            Monitoring();
            Invoke("TowerUpdate", .5f);
            towers.ForEach(x => x.Shooting());

            foreach (var mob in Spawner<Mob>.Array)
                mob.Movement();

            foreach (var tower in towers)
                tower.LookAtTarget();

            foreach (var bullet in Spawner<Bullet>.Array)
                bullet.Movement();
        }

        #endregion

        /* ********************************************************** */

        #region INITS

        void Routes_Init()
        {
            routes = new Vector3[routes_ph.childCount][];
            for (int i = 0; i < routes_ph.childCount; i++)
            {
                Transform route = routes_ph.GetChild(i);
                routes[i] = new Vector3[route.childCount];

                for (int j = 0; j < route.childCount; j++)
                    routes[i][j] = route.GetChild(j).position;
            }
        }

        void Mobs_Init()
        {
            mobs_ph = new GameObject("mob_ph").transform;
            GameObject prefab = Resources.Load<GameObject>("Prefabs/Game/mob_pref");

            Mob[] mobs = new Mob[10];
            for (int i = 0; i < mobs.Length; i++)
                mobs[i] = new Mob(Instantiate(prefab, mobs_ph));

            spawner_mob = new Spawner<Mob>(mobs);
        }

        void Bullets_Init()
        {
            bullets_ph = new GameObject("bullets_ph").transform;
            GameObject bullet_pref = Resources.Load<GameObject>("Prefabs/Game/bullet_pref");

            Bullet[] bullets = new Bullet[10];
            for (int i = 0; i < bullets.Length; i++)
                bullets[i] = new Bullet(Instantiate(bullet_pref, bullets_ph));

            new Spawner<Bullet>(bullets);
        }

        void Towers(bool show) => towers_ph.gameObject.SetActive(show);
        public void Tower_Add(TowerController tower)
        {
            if (!towers.Contains(tower)) towers.Add(tower);
        }
        public void Tower_Remove(TowerController tower)
        {
            if (towers.Contains(tower)) towers.Remove(tower);
        }

        #endregion

        public void Play()
        {
            data = new GameData();
            rules = new LevelRules(10);
            towers.ForEach(x => x.ToDefault());
            View.ShowSidebar();
            GameIsRunning = true;
        }
        public void Restart(bool isWin)
        {
            HideAllObjects();
            data.Score.SetProgress(isWin);
            View.ShowSidebar();
        }
        public void Quit() => Application.Quit();

        void HideAllObjects()
        {
            for (int i = 0; i < mobs_ph.childCount; i++)
            {
                GameObject obj = mobs_ph.GetChild(i).gameObject;
                if (obj.activeSelf) obj.SetActive(false);
            }

            for (int i = 0; i < bullets_ph.childCount; i++)
            {
                GameObject obj = bullets_ph.GetChild(i).gameObject;
                if (obj.activeSelf) obj.SetActive(false);
            }

        }

        public void BuildTower(TowerController tower)
        {
            data.Costs(tower.Stats.Cost);
            tower.OnModify();
        }

        public void GetProfit()
        {
            data.Profit();
            rules.AliveMobCount--;
            rules.TotalKills++;
        }
        public void GetDamage()
        {
            data.GetDamage();
            rules.AliveMobCount--;
        }

        void OnInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                {
                    TowerController tower = hit.collider.GetComponent<TowerController>();
                    if (tower == null) return;

                    View.ShowTowerPanel(tower);
                }
            }
        }
        void Monitoring()
        {
            if (data.Defence == 0 || rules.NoUnitLeft)
            {
                GameIsRunning = false;
                Result result = new Result()
                {
                    Win = data.Defence > 0 && rules.NoUnitLeft,
                    Kills = rules.TotalKills,
                    Score = data.Score.Current,
                    CleanWin = data.IsCleanWin
                };

                View.ShowResults(result);
            }

            if (rules.WaveIsClear()) rules = rules.NextWave();
        }
        void TowerUpdate()
        {
            if (towers == null || towers.Count == 0) return;

            foreach (var tower in towers)
            {
                if (!tower.IsActive) continue;

                tower.TargetMonitoring();

                Mob[] array = Spawner<Mob>.Array;
                List<Target> target_list = new List<Target>();
                for (int i = 0; i < array.Length; i++)
                {
                    if (!array[i].IsActive) continue;
                    if (tower.HaveTarget()) continue;

                    float dist = 0;
                    if (tower.IsAtRange(array[i].Pos, ref dist))
                        target_list.Add(new Target(array[i], dist));
                }

                if (target_list.Count != 0)
                {
                    target_list.Sort((s1, s2) => s1.dist.CompareTo(s2.dist));
                    tower.Target = target_list[0].target;
                }
            }
        }
        IEnumerator SpawnNewMobWave()
        {
            rules.LockNextSpawn();
            Vector3[] route = GetRout;
            for (int i = 0; i < rules.WaveMobCount; i++)
            {
                if (!GameIsRunning) yield break;

                spawner_mob.Spawn(route);
                yield return new WaitForSeconds(rules.Delay);
            }
        } 
    }
}