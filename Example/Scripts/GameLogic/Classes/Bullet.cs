﻿using UnityEngine;
using System.Collections;

namespace ASM.Project
{
    public class Bullet : MovableObject
    {
        #region VARIABLES

        public int Dmg { get; protected set; }

        Mob target;
        Vector3 endPos;

        Transform Cam => Camera.main.transform;

        #endregion

        /* ============================================== */

        public Bullet(GameObject obj)
        {
            this.obj = obj;
            Speed = 10;
            Show(false);
        }

        public override float RealSpeed() => Speed;

        public override void Movement()
        {
            if (!GameManager.Instance.GameIsRunning)
            {
                Show(false);
                return;
            }

            if (HaveTarget()) endPos = target.Pos + Vector3.up;

            float dist = Vector3.Distance(Pos, endPos);
            if (dist > .5f)
            {
                MoveTo(endPos);
                Tr.LookAt(Cam);
            }
            else
            {
                if (target != null) target.GetDamadge(Dmg);
                Show(false);
            }
        }

        public override void Show(bool show)
        {
            if (!show)
            {
                target = null;
                CanMove = false;
            }
            base.Show(show);
        }

        /* ============================================== */

        bool HaveTarget()
        {
            if (target != null && target.IsActive) return true;
            else
            {
                target = null;
                return false;
            }
        }

        public void SetTarget(Mob target, int dmg)
        {
            if (CanMove) return;

            this.target = target;
            Dmg = dmg;
            CanMove = true;
        }
    }
}