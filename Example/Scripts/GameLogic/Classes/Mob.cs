﻿using UnityEngine;
using System.Collections;

namespace ASM.Project
{
    /* =================================== */
    /* ENEMY UNIT : MAIN LOGIC             */
    /* =================================== */

    public class Mob : MovableObject
    {
        #region VARIABLES

        static GameManager Mng = GameManager.Instance;
        public int Hp { get; private set; }

        int index;

        #endregion

        /* ============================================== */

        public Mob(GameObject obj)
        {
            this.obj = obj;
            Hp = 100;
            Speed = 2;
            Show(false);
        }

        public override float RealSpeed() => Speed + Mng.SpeedModirier;

        public override void Movement()
        {
            if (!Mng.GameIsRunning)
            {
                Show(false);
                return;
            }

            if (Route == null || Route.Length == 0 || !IsActive) return;

            if (Pos == Route[index]) index += 1;

            if (index >= Route.Length)
            {
                Mng.GetDamage();
                Show(false);
            }
            else MoveTo(Route[index]);
        }

        public override void Show(bool show)
        {
            if (show)
            {
                index = 0;
                Hp = 100;
                Tr.position = Route[0];
                CanMove = true;
            }

            base.Show(show);
        }

        /* ============================================== */

        public void GetDamadge(int dmg)
        {
            Hp = Hp - dmg > 0 ? Hp - dmg : 0;
            if (Hp == 0)
            {
                Mng.GetProfit();
                Show(false);
            }
        }

        public bool SameObject(GameObject obj) => this.obj.Equals(obj);

    }
}