﻿namespace ASM.Project
{
    public enum LocalDataType
    {
        LOGIN,
        SCORES
    }

    public enum SoundType
    {
        MAIN,
        BUTTON_CLICK,
        UI_SHOW,
        GAME_OVER
    }
}