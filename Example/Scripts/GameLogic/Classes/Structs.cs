﻿using UnityEngine;

namespace ASM.Project
{
    /* =================================== */
    /* MAIN TOWER_CONTROLLER DATA STRUCT   */
    /* =================================== */

    public struct TowerStats
    {
        #region VARIABLES

        public int Level { get; private set; }
        public int Cost { get; private set; }
        public int Damage { get; private set; }
        public int Range { get; private set; }
        public float RotSpeed { get; private set; }

        #endregion

        public TowerStats Default()
        {
            Level = 0;
            Cost = 65;
            Damage = 6;
            Range = 7;
            RotSpeed = 1f;
            return this;
        }

        public TowerStats LevelUp()
        {
            Level++;
            Cost += 10;
            Damage += 4;
            Range += 2;
            RotSpeed += .5f;
            return this;
        }

        public TowerStats NextLevelValues()
        {
            TowerStats next = this;
            return next.LevelUp();
        }

        public string AsFormatedString => $"Level :\nCost :\nDamage :\nRange :\nRotSpeed :";
        public string NextLevelAsFormatedString()
        {
            TowerStats next = NextLevelValues();
            return $"{next.Level}\n{next.Cost}\n{next.Damage}\n{next.Range}\n{next.RotSpeed}";
        }
    }

    /* =================================== */
    /* MOB SPAWN RULES AND ACCOUNTING      */
    /* =================================== */

    public struct LevelRules
    {
        #region VARIABLES

        public readonly int WaveMax;
        public int WaveCurrent { get; set; }
        public float Delay { get; set; }
        public float SpeedModifier { get; set; }
        public int WaveMobCount { get; set; }
        public int AliveMobCount { get; set; }
        public bool CanSpawnNextWave { get; private set; }

        public int TotalKills { get; set; }

        const float MIN_DELAY = 1f;
        const float MAX_SPEED_MODIFIER = 3f;

        public bool NoUnitLeft => WaveCurrent == WaveMax && AliveMobCount == 0;
        public bool LockNextSpawn() => CanSpawnNextWave = false;

        #endregion

        /* ============================================== */

        public LevelRules(int waves)
        {
            WaveMax = waves;
            WaveCurrent = 1;
            Delay = 4f;
            SpeedModifier = 0f;
            WaveMobCount = 3;
            AliveMobCount = 3;
            CanSpawnNextWave = true;
            TotalKills = 0;
        }

        /* ============================================== */

        public LevelRules NextWave()
        {
            WaveCurrent++;
            Delay = Mathf.Clamp(Delay - .5f, MIN_DELAY, Delay);
            SpeedModifier = Mathf.Clamp(SpeedModifier + .4f, SpeedModifier, MAX_SPEED_MODIFIER);
            WaveMobCount++;
            AliveMobCount = WaveMobCount;
            CanSpawnNextWave = true;
            return this;
        }

        public bool WaveIsClear()
        {
            if (!CanSpawnNextWave && AliveMobCount == 0)
            {
                CanSpawnNextWave = true;
                return true;
            }

            return false;
        }
    }

    /* =================================== */
    /* USE FOR TRACKING MOB MOVEMENT       */
    /* =================================== */

    public struct Target
    {
        #region VARIABLES

        public readonly Mob target;
        public readonly float dist;

        #endregion

        /* ============================================== */

        public Target(Mob target, float dist)
        {
            this.target = target;
            this.dist = dist;
        }

        /* ============================================== */
    }

    /* =================================== */
    /* USE FOR TRANSMIT PARAMS TO VIEW     */
    /* =================================== */

    public struct Result
    {
        #region VARIABLES

        public bool Win { get; set; }
        public int Kills { get; set; }
        public int Score { get; set; }
        public bool CleanWin { get; set; }

        #endregion
    }
}