﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ASM.Project
{
    using Object = UnityEngine.Object;

    /* =================================== */
    /* DEFAULT NOT MONO_BEHAVIOR OBJECT    */
    /* =================================== */

    [Serializable]
    public class BaseObject
    {
        #region VARIABLES

        [Space(5)]
        [Header("From inheritance")]
        [SerializeField] protected GameObject obj;

        public Transform Tr => obj.transform;
        public Vector3 Pos => Tr.position;
        public bool IsActive => obj.activeSelf;

        #endregion

        public virtual void Show()
        {
            bool show = !IsActive;
            obj.SetActive(show);
        }
        public virtual void Show(bool show) => obj.SetActive(show);
        public virtual void Show(bool show, Vector3 atPos)
        {
            if (show) Tr.position = atPos;
            Show(show);
        }
        public virtual void SelfDestroy() => Object.Destroy(obj);
    }

    /* =================================== */
    /* DEFAULT MOVABLE OBJECT              */
    /* =================================== */

    public abstract class MovableObject : BaseObject
    {
        #region VARIABLES

        public bool CanMove { get; set; }
        public Vector3 EndPos { get; protected set; }
        public Vector3[] Route { get; protected set; }
        protected float Speed { get; set; }

        #endregion

        public abstract float RealSpeed();
        public abstract void Movement();
        protected virtual void MoveTo(Vector3 toPos)
        {
            if (!CanMove) return;
            Tr.position = Vector3.MoveTowards(Pos, toPos, Time.deltaTime * RealSpeed());
        }
        public virtual void Stop(bool show = true)
        {
            CanMove = false;
            Show(show);
        }
        public void SetRoute(Vector3[] route) => Route = route;
    }

    /* =================================== */
    /* DEFAULT UI PANELS PARENT            */
    /* =================================== */

    [Serializable]
    public class BaseUI : BaseObject
    {
        #region VARIABLES

        protected static SoundManager SoundMng => SoundManager.Instance;

        [SerializeField] protected Button base_btn;
        public Action BaseAct;

        #endregion

        /* ============================================== */

        public override void Show()
        {
            if (!IsActive) Tr.SetAsLastSibling();
            base.Show();
        }

        public override void Show(bool show)
        {
            if (show) Tr.SetAsLastSibling();
            obj.SetActive(show);
        }

        public override void Show(bool show, Vector3 atPos)
        {
            if (show) Tr.SetAsLastSibling();
            base.Show(show, atPos);
        }

        /* ============================================== */

        public virtual void Init()
        {
            base_btn.ClickAction(BaseAct_Default);
        }

        protected virtual void BaseAct_Default()
        {
            BaseAct?.Invoke();
            Show();
            BaseAct = () => { };
        }
    }
}