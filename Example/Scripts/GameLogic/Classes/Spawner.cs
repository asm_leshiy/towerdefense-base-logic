﻿using UnityEngine;
using System.Collections;

namespace ASM.Project
{
    /* =================================== */
    /* MOBS AND BULLETS SPAWN CLASS        */
    /* =================================== */

    public class Spawner<T> where T : MovableObject
    {
        #region VARIABLES

        static GameManager Mng => GameManager.Instance;

        public static T[] Array { get; private set; }
        public static int Waves { get; private set; }

        int count;
        IEnumerator process;
        WaitForSeconds Delay = new WaitForSeconds(1f);

        bool IsMob => typeof(T) == typeof(Mob);
        bool IsBullet => typeof(T) == typeof(Bullet);

        #endregion

        /* ============================================== */

        public Spawner() { }

        public Spawner(T[] array)
        {
            Array = array;
            count = array.Length;
        }

        /* ============================================== */

        bool CanSpawn()
        {
            if (Array == null || Array.Length == 0 || process != null) return false;
            else return true;
        }

        public void Spawn(Vector3[] route)
        {
            if (!CanSpawn()) return;

            for (int i = 0; i < count; i++)
            {
                if (!Array[i].IsActive)
                {
                    Array[i].SetRoute(route);
                    Array[i].Show(true);
                    break;
                }
            }
        }

        public void Spawn(TowerController tower)
        {
            if (!CanSpawn() || !IsBullet) return;

            count = Array.Length;
            for (int i = 0; i < count; i++)
            {
                if (!Array[i].IsActive)
                {
                    Array[i].Show(true, tower.SpawnPos);
                    Bullet bullet = Array[i] as Bullet;
                    bullet.SetTarget(tower.Target, tower.Stats.Damage);
                    break;
                }
            }

            process = Cooldown();
            Mng.StartCoroutine(process);
        }

        IEnumerator Cooldown()
        {
            yield return Delay;
            process = null;
        }
    }
}