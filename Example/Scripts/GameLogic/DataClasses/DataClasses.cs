﻿using UnityEngine;
using System.Collections;

namespace ASM.Project
{
    /* =================================== */
    /* MAIN GAME_MANAGER DATA CLASS        */
    /* =================================== */

    public class GameData
    {
        #region VARIABLES

        public int Gold { get; set; }
        public int Defence { get; set; }
        public GameScore Score { get; private set; }

        const int DEFAULT_GOLD = 230;
        const int DEFENCE_MAX = 100;

        public bool IsCleanWin => Defence == DEFENCE_MAX;

        #endregion

        /* ============================================== */

        public GameData()
        {
            Gold = DEFAULT_GOLD;
            Defence = DEFENCE_MAX;
            Score = new GameScore();
        }

        /* ============================================== */

        public void Costs(int value) => Gold = Gold - value > 0 ? Gold - value : 0;
        public void Profit() => Gold += Random.Range(20, 30);
        public void GetDamage() => Defence = Defence - 20 >= 0 ? Defence - 20 : 0;
        public int RealScore(int kills) => Score.KillsToScore(kills);
    }

    /* =================================== */
    /* USER SCORE CONTAINER IN GAME_DATA   */
    /* =================================== */

    public class GameScore
    {
        #region VARIABLES

        static int total;
        public int Total => total;
        public int Current { get; private set; }

        #endregion

        public int KillsToScore(int kills)
        {
            Current = kills * 25;
            return Current;
        }

        public void SetProgress(bool isWin)
        {
            if (!isWin) return;

            total += Current;
        }
    }
}