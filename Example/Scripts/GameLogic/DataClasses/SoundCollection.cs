﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ASM.Project.Data
{
    [CreateAssetMenu(fileName = "SoundCollection", menuName = "ASM.Project/SoundCollection", order = 1)]
    [System.Serializable]
    public class SoundCollection : ScriptableObject
    {
        public AudioClip[] Collection;
    }
}