﻿using System;
using UnityEngine.UI;

namespace ASM.Project
{
    /* ============================= */
    /* USER INTERFACE EXTENSION      */
    /* ============================= */

    public static class UiUtil
    {
        public static Button ClickAction(this Button btn, Action action)
        {
            btn.onClick.RemoveAllListeners();
            btn.onClick.AddListener(() =>
            {
                SoundManager.Instance.Play(SoundType.BUTTON_CLICK);
                action?.Invoke();
            });
            return btn;
        }
    }
}